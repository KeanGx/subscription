# Subscription Service

Subscription Service handles the subscription to multiple newsletter campaigns.

## Clone the Repository
Use this link to clone the [repository](https://bitbucket.org/KeanGx/subscription/src/master/) to your local.

## Run the APP
For example, to run the app using IntelliJ you could import the project as a Module From Existing Sources after cloning the repository in your local.
The app has been built with Maven and SpringBoot.
After importing it in your IDE be sure to download all dependencies, and you should be able to run the app with spring/maven.

Within the application.properties file you'll be able to configure the database properties needed to connect to your local database for the app.

For the Database a schema called adidas is needed and then create the following table:

```mysql
CREATE TABLE `subscription` (
`id` int unsigned NOT NULL AUTO_INCREMENT,
`email` varchar(240) NOT NULL,
`first_name` varchar(45) DEFAULT NULL,
`gender` enum('FEMALE','MALE','OTHER') DEFAULT NULL,
`birth_date` date DEFAULT NULL,
`consent` bit(1) NOT NULL,
`newsletter_id` int NOT NULL,
`created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

Sadly I have no experience with Docker to be able to hand off the app within a container ready to run.

## Use the APP
To Start and be able to see the Swagger Documentation created for the Service, go to this url http://localhost:8880/swagger-ui.html.
With this you'll be able to see what endpoints are available and how to use them.


#### In the CI Directory you'll find a few configuration files for deploying the microserrvice into a kubernetes cluster in an AWS EKS Cluster.
#### Also in the CI Directory you'll find a JenkinsFile with a possible CI/CD pipeline for the app.

## Main Dependecies

* spring-boot-starter-data-jpa: This is primarily to handle all database operations and be able to retrieve, persist and delete the subscriptions
* mysql-connector-java: This is needed to connect the App to the database.
* spring-boot-starter-web: This is used to be able to create the request/endpoints of the App
* spring-cloud-starter-netflix-ribbon: This is used to create the ribbons to be able to connect the applications with each other.
* spring-boot-starter-test: This is used for JUnit
* spring-boot-starter-validation: This is used for all the business validation checks added.
* mapstruct: This is used to map all the data either from Entity to a Data Transfer Object or viceversa without the need to manually mapped them and be able to reduce boiler plate.
* lombok: Just like the previous one, Lombok helps use reduce the boiler plate of the getters/setters/constructors of each object,
* springfox-swagger2/springfox-swagger-ui: Both of this are used to be able to show a documentation via Swagger.
* spring-cloud-starter-contract-stub-runner/spring-cloud-starter-contract-verifier: This could be used in the future to add Spring Cloud Contracts to the app and be able to rn integrations tests between the multiple microservices.




