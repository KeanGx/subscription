package com.adidas.subscription.services;

import com.adidas.subscription.BaseTestClass;
import com.adidas.subscription.clients.EmailClient;
import com.adidas.subscription.dtos.requests.EmailNotificationRequest;
import com.adidas.subscription.exceptions.SendNotificationException;
import com.adidas.subscription.utils.enums.EmailTopic;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.ConstraintViolationException;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class EmailServiceTest extends BaseTestClass {

	@Autowired
	private EmailService emailService;

	@MockBean
	private EmailClient emailClient;

	@Test
	void sendEmailNotification() throws Exception {
		String email = "email@adidas.com";
		EmailTopic topic = EmailTopic.SUBSCRIPTION_CREATED;

		EmailNotificationRequest expectedRequest = new EmailNotificationRequest();
		expectedRequest.setEmail(email);
		expectedRequest.setTopic(topic);

		emailService.sendEmailNotification(email, topic);

		verify(emailClient, times(1)).sendNotification(expectedRequest);
	}

	@Test
	void sendNotification() throws Exception {
		EmailNotificationRequest request = new EmailNotificationRequest();
		request.setEmail("email@adidas.com");
		request.setTopic(EmailTopic.SUBSCRIPTION_CANCELED);

		emailService.sendEmailNotification(request);
		verify(emailClient, times(1)).sendNotification(request);
	}

	@Test
	void sendNotification_requirementsFail() {
		EmailNotificationRequest request = new EmailNotificationRequest();
		request.setEmail("email.adidas.com");
		request.setTopic(EmailTopic.SUBSCRIPTION_CANCELED);

		assertThrows(ConstraintViolationException.class, () -> {
			emailService.sendEmailNotification(request);
		});
	}

	@Test
	void sendNotification_fail() {
		EmailNotificationRequest request = new EmailNotificationRequest();
		request.setEmail("email@adidas.com");
		request.setTopic(EmailTopic.SUBSCRIPTION_CREATED);

		doThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST))
				.when(emailClient).sendNotification(request);

		assertThrows(SendNotificationException.class, () -> emailService.sendEmailNotification(request));
	}

	@Test
	void buildNotificationRequest() {
		String email = "email@adidas.com";
		EmailTopic topic = EmailTopic.SUBSCRIPTION_CREATED;
		EmailNotificationRequest request = emailService.buildEmailNotificationRequest(email, topic);

		assertNotNull(request);
		assertAll("request",
				() -> Assertions.assertEquals(email, request.getEmail()),
				() -> Assertions.assertEquals(topic, request.getTopic())
		);
	}
}