package com.adidas.subscription.services;

import com.adidas.subscription.BaseTestClass;
import com.adidas.subscription.dtos.responses.SubscriptionData;
import com.adidas.subscription.dtos.requests.SubscriptionRequest;
import com.adidas.subscription.entities.Subscription;
import com.adidas.subscription.exceptions.SubscriptionAlreadyExists;
import com.adidas.subscription.exceptions.SubscriptionNotFound;
import com.adidas.subscription.repositories.SubscriptionRepository;
import com.adidas.subscription.utils.enums.EmailTopic;
import com.adidas.subscription.utils.enums.GenderTypes;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SubscriptionServiceTest extends BaseTestClass {

	@Autowired
	private SubscriptionService subscriptionService;

	@MockBean
	EmailService emailService;

	@MockBean
	private SubscriptionRepository subscriptionRepository;

	@Test
	void createSubscription_basicInfoRequirements() {
		try {
			SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
			subscriptionRequest.setEmail("email@adidas.com");
			subscriptionRequest.setBirthdate(new Date());
			subscriptionRequest.setConsent(true);
			subscriptionRequest.setNewsletterId(514L);

			Subscription mockSubscription = new Subscription();
			mockSubscription.setId(515L);
			mockSubscription.setEmail(subscriptionRequest.getEmail());

			when(
					subscriptionRepository.findByEmailAndNewsletterId(
							subscriptionRequest.getEmail(),
							subscriptionRequest.getNewsletterId()
					)
			).thenReturn(null);
			when(subscriptionRepository.save(any(Subscription.class))).thenReturn(mockSubscription);

			Long subscriptionId = subscriptionService.createSubscription(subscriptionRequest);
			Assert.assertEquals(mockSubscription.getId(), subscriptionId);

			verify(emailService, times(1))
					.sendEmailNotification(mockSubscription.getEmail(), EmailTopic.SUBSCRIPTION_CREATED);
		} catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void createSubscription_basicRequirementsNotFulfilled() {
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		subscriptionRequest.setEmail("email.adidas.com");
		subscriptionRequest.setBirthdate(new Date());
		subscriptionRequest.setConsent(true);
		subscriptionRequest.setNewsletterId(514L);

		assertThrows(ConstraintViolationException.class, () -> {
			subscriptionService.createSubscription(subscriptionRequest);
		});
	}


	@Test
	void createSubscription_SubscriptionAlreadyExists() {
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		subscriptionRequest.setEmail("email@adidas.com");
		subscriptionRequest.setBirthdate(new Date());
		subscriptionRequest.setConsent(true);
		subscriptionRequest.setNewsletterId(514L);

		Subscription mockSubscription = new Subscription();
		mockSubscription.setId(515L);

		when(
				subscriptionRepository.findByEmailAndNewsletterId(
						subscriptionRequest.getEmail(),
						subscriptionRequest.getNewsletterId()
				)
		).thenReturn(mockSubscription);

		assertThrows(SubscriptionAlreadyExists.class, () -> {
			subscriptionService.createSubscription(subscriptionRequest);
		});
	}

	@Test
	void findSubscriptionByEmailAndNewsletterCampaign() {
		String email = "email@adidas.com";
		Long newsletterId = 54L;

		Subscription mockSubscription = new Subscription();
		mockSubscription.setId(515L);
		mockSubscription.setEmail(email);
		mockSubscription.setNewsletterId(newsletterId);
		mockSubscription.setConsent(true);
		mockSubscription.setBirthdate(new Date());
		mockSubscription.setGender(GenderTypes.MALE);

		when(subscriptionRepository.findByEmailAndNewsletterId(email, newsletterId)).thenReturn(mockSubscription);

		SubscriptionData subscriptionData = subscriptionService.findSubscription(email, newsletterId);
		Assertions.assertNotNull(subscriptionData);
		assertAll("subscriptionData",
				() -> assertEquals(mockSubscription.getId(), subscriptionData.getId()),
				() -> Assertions.assertEquals(mockSubscription.getEmail(), subscriptionData.getEmail()),
				() -> Assertions.assertEquals(mockSubscription.getNewsletterId(), subscriptionData.getNewsletterId()),
				() -> Assertions.assertEquals(mockSubscription.isConsent(), subscriptionData.isConsent()),
				() -> Assertions.assertEquals(mockSubscription.getBirthdate(), subscriptionData.getBirthdate()),
				() -> Assertions.assertEquals(mockSubscription.getGender(), subscriptionData.getGender())
		);
	}

	@Test
	void findSubscriptionByEmailAndNewsletterCampaign_notFound() {
		String email = "email@adidas.com";
		Long newsletterId = 54L;

		when(subscriptionRepository.findByEmailAndNewsletterId(email, newsletterId)).thenReturn(null);

		SubscriptionData subscriptionData = subscriptionService.findSubscription(email, newsletterId);
		Assertions.assertNull(subscriptionData);
	}

	@Test
	void findSubscriptionByEmailAndNewsletterCampaign_validationFail() {
		String email = "email.adidas.com";

		assertThrows(ConstraintViolationException.class, () -> {
			subscriptionService.findSubscription(email, null);
		});
	}


	@Test
	void findSubscriptionById() throws SubscriptionNotFound {
		String email = "email@adidas.com";
		Long newsletterId = 54L;

		Subscription mockSubscription = new Subscription();
		mockSubscription.setId(515L);
		mockSubscription.setEmail(email);
		mockSubscription.setNewsletterId(newsletterId);
		mockSubscription.setConsent(true);
		mockSubscription.setBirthdate(new Date());
		mockSubscription.setGender(GenderTypes.MALE);

		when(subscriptionRepository.findById(mockSubscription.getId())).thenReturn(Optional.of(mockSubscription));

		SubscriptionData subscriptionData = subscriptionService.findSubscription(mockSubscription.getId());
		Assertions.assertNotNull(subscriptionData);
		assertAll("subscriptionData",
				() -> assertEquals(mockSubscription.getId(), subscriptionData.getId()),
				() -> Assertions.assertEquals(mockSubscription.getEmail(), subscriptionData.getEmail()),
				() -> Assertions.assertEquals(mockSubscription.getNewsletterId(), subscriptionData.getNewsletterId()),
				() -> Assertions.assertEquals(mockSubscription.isConsent(), subscriptionData.isConsent()),
				() -> Assertions.assertEquals(mockSubscription.getBirthdate(), subscriptionData.getBirthdate()),
				() -> Assertions.assertEquals(mockSubscription.getGender(), subscriptionData.getGender())
		);
	}

	@Test
	void findSubscriptionById_notFound() {
		Long subscriptionId = 81L;
		when(subscriptionRepository.findById(subscriptionId)).thenReturn(Optional.empty());

		assertThrows(SubscriptionNotFound.class, () -> {
			subscriptionService.findSubscription(subscriptionId);
		});
	}

	@Test
	void findSubscriptionById_validationFail() {
		assertThrows(ConstraintViolationException.class, () -> {
			subscriptionService.findSubscription(null);
		});
	}

	@Test
	void validateSubscriptionRequest() throws SubscriptionAlreadyExists {
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		subscriptionRequest.setEmail("email@adidas.com");
		subscriptionRequest.setBirthdate(new Date());
		subscriptionRequest.setConsent(true);
		subscriptionRequest.setNewsletterId(514L);

		when(subscriptionRepository.findByEmailAndNewsletterId(subscriptionRequest.getEmail(), subscriptionRequest.getNewsletterId())).thenReturn(null);
		subscriptionService.validateSubscriptionRequest(subscriptionRequest);
	}

	@Test
	void validateSubscriptionRequest_validationFail() {
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		subscriptionRequest.setEmail("email.adidas.com");

		assertThrows(ConstraintViolationException.class, () -> {
			subscriptionService.validateSubscriptionRequest(subscriptionRequest);
		});
	}

	@Test
	void validateSubscriptionRequest_subscriptionAlreadyExists() {
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		subscriptionRequest.setEmail("email@adidas.com");
		subscriptionRequest.setBirthdate(new Date());
		subscriptionRequest.setConsent(true);
		subscriptionRequest.setNewsletterId(514L);

		Subscription mockSubscription = new Subscription();
		mockSubscription.setId(515L);
		mockSubscription.setEmail(subscriptionRequest.getEmail());
		mockSubscription.setNewsletterId(subscriptionRequest.getNewsletterId());
		mockSubscription.setConsent(subscriptionRequest.getConsent());
		mockSubscription.setBirthdate(subscriptionRequest.getBirthdate());

		when(subscriptionRepository.findByEmailAndNewsletterId(subscriptionRequest.getEmail(), subscriptionRequest.getNewsletterId()))
				.thenReturn(mockSubscription);

		assertThrows(SubscriptionAlreadyExists.class, () -> {
			subscriptionService.validateSubscriptionRequest(subscriptionRequest);
		});
	}
}