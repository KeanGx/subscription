package com.adidas.subscription.exceptions;

import com.adidas.subscription.utils.constants.Messages;

public class SendNotificationException extends Exception {
	private static final String MESSAGE = Messages.SEND_NOTIFICATION_ERROR;

	public SendNotificationException() {
		super(MESSAGE);
	}

}
