package com.adidas.subscription.exceptions;

import com.adidas.subscription.utils.constants.Messages;

public class SubscriptionNotFound extends Exception {
	private static final String MESSAGE = Messages.SUBSCRIPTION_NOT_FOUND;

	public SubscriptionNotFound() {
		super(MESSAGE);
	}


}
