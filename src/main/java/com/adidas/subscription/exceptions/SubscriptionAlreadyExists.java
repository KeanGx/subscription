package com.adidas.subscription.exceptions;

import com.adidas.subscription.utils.constants.Messages;

public class SubscriptionAlreadyExists extends Exception {
	private static final String MESSAGE = Messages.SUBSCRIPTION_ALREADY_EXISTS;

	public SubscriptionAlreadyExists() {
		super(MESSAGE);
	}


}
