package com.adidas.subscription.exceptions;

import com.adidas.subscription.utils.constants.Messages;

import static java.lang.String.format;

public class CreateSubscriptionException extends Exception {
	private static final String MESSAGE = Messages.CREATE_SUBSCRIPTION_EXCEPTION;
	private static final String ERROR = Messages.ERROR;

	public CreateSubscriptionException() {
		super(MESSAGE);
	}

	public CreateSubscriptionException(String error) {
		super(MESSAGE.concat(format(ERROR, error)));
	}

}
