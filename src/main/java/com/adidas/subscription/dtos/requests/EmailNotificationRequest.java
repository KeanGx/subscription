package com.adidas.subscription.dtos.requests;

import com.adidas.subscription.utils.constraints.Enum;
import com.adidas.subscription.utils.enums.EmailTopic;
import com.adidas.subscription.utils.enums.GenderTypes;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailNotificationRequest {

	@Email(message = "Email is required and follow the correct format of an email")
	@ApiModelProperty(required = true)
	private String email;

	@NotNull
	@ApiModelProperty(required = true)
	private EmailTopic topic;

}
