package com.adidas.subscription.dtos.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class ApiResponseDTO<T> {

	@ApiModelProperty(required = true)
	private HttpStatus status;
	private String error;
	private Object detail;
	private T data;

	public ApiResponseDTO(HttpStatus status) {
		this.status = status;
	}

	public ApiResponseDTO(HttpStatus status, T data) {
		this.status = status;
		this.data = data;
	}

	public ApiResponseDTO(HttpStatus status, Exception exception) {
		this.status = status;
		this.error = exception.getClass().getSimpleName();
		this.detail = exception.getMessage();
	}


	public ApiResponseDTO(HttpStatus status, String error, T detail) {
		this.status = status;
		this.error = error;
		this.detail = detail;
	}

}