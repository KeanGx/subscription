package com.adidas.subscription.dtos.responses;

import com.adidas.subscription.utils.enums.GenderTypes;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class SubscriptionData {

	@ApiModelProperty(required = true)
	private Long id;

	@ApiModelProperty(required = true)
	private String email;

	@ApiModelProperty(required = true)
	private Date birthdate;

	@ApiModelProperty(required = true)
	private boolean consent;

	@ApiModelProperty(required = true)
	private Long newsletterId;

	private String firstName;
	private GenderTypes gender;
}
