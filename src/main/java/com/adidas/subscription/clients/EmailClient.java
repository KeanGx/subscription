package com.adidas.subscription.clients;

import com.adidas.subscription.dtos.requests.EmailNotificationRequest;
import com.adidas.subscription.utils.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RibbonClient(name = "email")
@Component
public class EmailClient {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${email.api.url:http://email/}")
	private String emailClientUrl;

	public void sendNotification(EmailNotificationRequest notificationRequest) {
		UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(emailClientUrl + "/v1.0/notifications");
		HttpUtils.sendRequest(uri.toUriString(), notificationRequest, HttpMethod.POST, Void.class, restTemplate);
	}

}
