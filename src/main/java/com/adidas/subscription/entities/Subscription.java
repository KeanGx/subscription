package com.adidas.subscription.entities;

import com.adidas.subscription.utils.enums.GenderTypes;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "subscription", catalog = "adidas")
public class Subscription {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "gender")
	@Enumerated(EnumType.STRING)
	private GenderTypes gender;

	@Column(name = "birth_date", nullable = false)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date birthdate;

	@Column(name = "consent", nullable = false)
	private boolean consent;

	@Column(name = "newsletter_id", nullable = false)
	private Long newsletterId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created")
	private Date created;
}
