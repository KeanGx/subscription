package com.adidas.subscription.services;

import com.adidas.subscription.clients.EmailClient;
import com.adidas.subscription.dtos.requests.EmailNotificationRequest;
import com.adidas.subscription.exceptions.SendNotificationException;
import com.adidas.subscription.utils.enums.EmailTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import static org.springframework.util.CollectionUtils.isEmpty;

@Service
@Transactional
@Validated
public class EmailService {

	@Autowired
	private EmailClient emailClient;

	private final Logger log = LoggerFactory.getLogger(EmailService.class);

	public void sendEmailNotification(@Email String email, @NotNull EmailTopic emailTopic) {
		try {
			EmailNotificationRequest request = buildEmailNotificationRequest(email, emailTopic);
			sendEmailNotification(request);
		} catch (Exception e) {
			log.error("Error sending email notification. Topic: {}. Error: {}", emailTopic, e.getLocalizedMessage());
		}
	}

	public void sendEmailNotification(EmailNotificationRequest notificationRequest) throws SendNotificationException {
		try {
			emailClient.sendNotification(notificationRequest);
		} catch (Exception e) {
			log.error("Error sending email notification. Error: {}", e.getLocalizedMessage());
			throw new SendNotificationException();
		}
	}

	protected EmailNotificationRequest buildEmailNotificationRequest(@Email String email, @NotNull EmailTopic emailTopic) {
		EmailNotificationRequest request = null;
		try {
			request = new EmailNotificationRequest();
			request.setEmail(email);
			request.setTopic(emailTopic);
		} catch (Exception e) {
			log.error("Error building email notification request. Error: {}", e.getLocalizedMessage());
		}

		return request;
	}
}
