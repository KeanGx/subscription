package com.adidas.subscription.services;

import com.adidas.subscription.dtos.responses.SubscriptionData;
import com.adidas.subscription.dtos.requests.SubscriptionRequest;
import com.adidas.subscription.entities.Subscription;
import com.adidas.subscription.exceptions.CreateSubscriptionException;
import com.adidas.subscription.exceptions.SubscriptionAlreadyExists;
import com.adidas.subscription.exceptions.SubscriptionNotFound;
import com.adidas.subscription.mappers.SubscriptionMapper;
import com.adidas.subscription.repositories.SubscriptionRepository;
import com.adidas.subscription.utils.enums.EmailTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.concurrent.CompletableFuture.runAsync;
import static org.springframework.util.CollectionUtils.isEmpty;

@Service
@Transactional
@Validated
public class SubscriptionService implements Serializable {

	@Autowired
	private EmailService emailService;

	@Autowired
	private SubscriptionRepository subscriptionRepository;

	@Autowired
	private SubscriptionMapper subscriptionMapper;

	public Long createSubscription(@NotNull @Valid SubscriptionRequest subscriptionRequest) throws Exception {
		Long subscriptionId = null;
		try {
			validateSubscriptionRequest(subscriptionRequest);
			Subscription subscription = subscriptionMapper.requestToEntity(subscriptionRequest);
			subscriptionId = saveSubscription(subscription);

			runAsync(() -> emailService.sendEmailNotification(subscriptionRequest.getEmail(), EmailTopic.SUBSCRIPTION_CREATED));
		} catch (Exception e) {
			throw new CreateSubscriptionException(e.getLocalizedMessage());
		}

		return subscriptionId;
	}

	public SubscriptionData findSubscription(@Email String email, @NotNull Long newsletterId) {
		SubscriptionData subscriptionData = null;
		Subscription subscription = subscriptionRepository.findByEmailAndNewsletterId(email, newsletterId);
		if(subscription != null) {
			subscriptionData = subscriptionMapper.entityToDTO(subscription);
		}

		return subscriptionData;
	}

	public SubscriptionData findSubscription(@NotNull Long subscriptionId) throws SubscriptionNotFound {
		Optional<Subscription> subscription = subscriptionRepository.findById(subscriptionId);
		if(!subscription.isPresent()) {
			throw new SubscriptionNotFound();
		}

		return subscriptionMapper.entityToDTO(subscription.get());
	}

	protected void validateSubscriptionRequest(@NotNull @Valid SubscriptionRequest subscriptionRequest) throws SubscriptionAlreadyExists {
		SubscriptionData subscription = findSubscription(subscriptionRequest.getEmail(), subscriptionRequest.getNewsletterId());
		if(subscription != null) {
			throw new SubscriptionAlreadyExists();
		}
	}

	protected Long saveSubscription(@NotNull Subscription subscription) {
		subscription.setCreated(new Date());
		Subscription persistedSubscription = subscriptionRepository.save(subscription);
		return persistedSubscription.getId();
	}

	public List<SubscriptionData> searchSubscriptions(List<Long> ids, List<String> emails, List<Long> newsletterIds) {
		List<SubscriptionData> subscriptionDataList = emptyList();
		List<Subscription> subscriptions = subscriptionRepository.findAll(((root, query, criteriaBuilder) -> {
			List<Predicate> predicates = new ArrayList<>();

			if (!isEmpty(ids)) {
				predicates.add(
						criteriaBuilder.in(root.get("id")).value(ids)
				);
			}

			if (!isEmpty(emails)) {
				predicates.add(
						criteriaBuilder.in(root.get("email")).value(emails)
				);
			}

			if (!isEmpty(newsletterIds)) {
				predicates.add(
						criteriaBuilder.in(root.get("newsletterId")).value(newsletterIds)
				);
			}

			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
		}));

		if(!isEmpty(subscriptions)) {
			subscriptionDataList = subscriptionMapper.entityListToDTO(subscriptions);
		}

		return subscriptionDataList;
	}

	public void cancelSubscription(@NotNull Long subscriptionId) throws SubscriptionNotFound {
		SubscriptionData subscription = findSubscription(subscriptionId);
		subscriptionRepository.deleteById(subscriptionId);

		runAsync(() -> emailService.sendEmailNotification(subscription.getEmail(), EmailTopic.SUBSCRIPTION_CANCELED));
	}
}
