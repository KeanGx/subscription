package com.adidas.subscription.mappers;

import com.adidas.subscription.dtos.responses.SubscriptionData;
import com.adidas.subscription.dtos.requests.SubscriptionRequest;
import com.adidas.subscription.entities.Subscription;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", implementationName = "SubscriptionMapperImpl")
public interface SubscriptionMapper {
	SubscriptionData entityToDTO (Subscription subscription);
	Subscription requestToEntity(SubscriptionRequest subscriptionRequest);

	List<SubscriptionData> entityListToDTO(List<Subscription> subscriptionList);
}
