package com.adidas.subscription.repositories;

import com.adidas.subscription.entities.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long>, JpaSpecificationExecutor<Subscription> {
	Subscription findByEmailAndNewsletterId(String email, Long newsletterId);
}
