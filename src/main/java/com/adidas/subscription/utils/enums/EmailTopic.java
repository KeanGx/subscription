package com.adidas.subscription.utils.enums;

public enum EmailTopic {
	SUBSCRIPTION_CREATED, SUBSCRIPTION_CANCELED;
}
