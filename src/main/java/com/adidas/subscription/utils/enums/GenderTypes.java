package com.adidas.subscription.utils.enums;

public enum GenderTypes {
	FEMALE,	MALE, OTHER;
}
