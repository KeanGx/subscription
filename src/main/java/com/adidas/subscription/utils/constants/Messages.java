package com.adidas.subscription.utils.constants;

public class Messages {
	public static final String ERROR = "Error: %s";
	public static final String SUBSCRIPTION_ALREADY_EXISTS = "A Subscription for the specified email and newsletter campaign already exists.";
	public static final String CREATE_SUBSCRIPTION_EXCEPTION = "Failed to create subscription.";
	public static final String SUBSCRIPTION_NOT_FOUND = "The subscription specified was not found.";
	public static final String SUBSCRIPTION_CREATED_EMAIL_MESSAGE = "You have successfully subscribe to our newsletter.";
	public static final String SUBSCRIPTION_CREATED_EMAIL_SUBJECT = "Welcome to the Adidas Club";
	public static final String SEND_NOTIFICATION_ERROR = "There was an error while sending an email notification.";

}
